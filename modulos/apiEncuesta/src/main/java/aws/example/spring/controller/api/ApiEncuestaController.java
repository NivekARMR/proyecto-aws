package aws.example.spring.controller.api;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import aws.example.spring.dto.RespuestaApi;
import aws.example.spring.model.Encuesta;
import aws.example.spring.service.IEncuestaService;

@RestController
@CrossOrigin
public class ApiEncuestaController {
	
	private static final Logger log = LoggerFactory.getLogger(ApiEncuestaController.class);
	
	@Autowired
	private IEncuestaService encuestaService;
	
	@GetMapping(value="api/encuestas", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarTodos(Pageable pageable){
		try {
			//User usuario = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			if (pageable != null) {
				log.info("Encuestas listando todos paginado...");
				return new ResponseEntity<Page<Encuesta>>(encuestaService.listarTodos(pageable), HttpStatus.OK);
			} else {
				log.info("Encuestas listando todos...");
				return new ResponseEntity<List<Encuesta>>(encuestaService.listarTodos(), HttpStatus.OK);
			}
		}catch(Exception e) {
			log.error("Error: ", e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping(value="api/encuestas/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> listarPorId(@PathVariable int id){
		try {
			//User usuario = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
			log.info("Encuesta listado por id = " + id + "...");
			return new ResponseEntity<Encuesta>(encuestaService.listar(id), HttpStatus.OK);
		}catch(Exception e) {
			log.error("Error: ", e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PostMapping(value="api/encuestas", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaApi> registrar(@RequestBody Encuesta encuesta){
		try {
			log.info("Encuesta recibida para registrar: " + encuesta);			
			encuestaService.registrar(encuesta);
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""), HttpStatus.OK);
		}catch(Exception e) {
			log.error("Error: ", e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@PutMapping(value="api/encuestas", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaApi> modificar(@RequestBody Encuesta encuesta){
		try {
			log.info("Encuesta recibida para modificar: " + encuesta);
			encuestaService.modificar(encuesta);
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""), HttpStatus.OK);
		}catch(Exception e) {
			log.error("Error: ", e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@DeleteMapping(value="api/encuestas/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<RespuestaApi> eliminar(@PathVariable int id){
		try {
			encuestaService.eliminar(id);
			return new ResponseEntity<RespuestaApi>(new RespuestaApi("OK",""), HttpStatus.OK);
		}catch(Exception e) {
			log.error("Error: ",e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}