package aws.example.spring.service.impl;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Service;

import aws.example.spring.dao.IEncuestaDao;
import aws.example.spring.model.Encuesta;
import aws.example.spring.service.IEncuestaService;

@Service
public class EncuestaServiceImpl implements IEncuestaService {
	
	private static final Logger LOG = Logger.getLogger(EncuestaServiceImpl.class);

	@Autowired
	private IEncuestaDao encuestaDao;
	
	//@Autowired
	//private INotificacionSNS notificacionSNS;
	
	@Override
	public List<Encuesta> listarTodos() {
		/*
		boolean isAdmin = false;
		if(ltaRoles != null && ltaRoles.size() > 0) {
			for(GrantedAuthority rol : ltaRoles) {
				if("ROLE_ADMIN".equalsIgnoreCase(rol.getAuthority())) {
					isAdmin = true;
					break;
				}
			}
		}
		*/
		//if(isAdmin) {
		//	return encuestaDao.obtenerEncuestas(pageable);
		LOG.info("Listando todas las encuestas");
		return encuestaDao.findAll();
		//}
		//return encuestaDao.obtenerFeedBacksPorUsuario(pageable, usuario);
	}

	@Override
	public Page<Encuesta> listarTodos(Pageable pageable/*, String usuario, Collection<GrantedAuthority> ltaRoles*/) {
		/*
		boolean isAdmin = false;
		if(ltaRoles != null && ltaRoles.size() > 0) {
			for(GrantedAuthority rol : ltaRoles) {
				if("ROLE_ADMIN".equalsIgnoreCase(rol.getAuthority())) {
					isAdmin = true;
					break;
				}
			}
		}
		*/
		//if(isAdmin) {
		//	return encuestaDao.obtenerEncuestas(pageable);
		LOG.info("Listando pagina de todas las encuestas");
		return encuestaDao.findAll(pageable);
		//}
		//return encuestaDao.obtenerFeedBacksPorUsuario(pageable, usuario);
	}

	@Override
	public Encuesta listar(Integer id) {
		/*
		boolean isAdmin = false;
		if(ltaRoles != null && ltaRoles.size() > 0) {
			for(GrantedAuthority rol : ltaRoles) {
				if("ROLE_ADMIN".equalsIgnoreCase(rol.getAuthority())) {
					isAdmin = true;
					break;
				}
			}
		}
		*/
		//if(isAdmin) {
		//	return encuestaDao.obtenerEncuestas(pageable);
		LOG.info("Listando encuesta por id = " + id);
		return encuestaDao.findOne(id);
		//}
		//return encuestaDao.obtenerFeedBacksPorUsuario(pageable, usuario);
	}

	@Override
	public void registrar(Encuesta encuesta) {
				
		LOG.info("Registrando encuesta: " + encuesta);

		encuestaDao.save(encuesta);
	}

	@Override
	public void modificar(Encuesta encuesta) {
		
		LOG.info("Modificando encuesta: " + encuesta);

		encuestaDao.save(encuesta);
	}

	@Override
	public void eliminar(int id) {
		LOG.info("Eliminando encuesta con id=" + id);
		encuestaDao.delete(id);
	}

}
