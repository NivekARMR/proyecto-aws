package aws.example.spring.service;

import aws.example.spring.dto.RenewPasswordFirstDTO;
import aws.example.spring.dto.RespuestaApi;
import aws.example.spring.dto.UpdatePasswordDTO;

public interface SecurityService {

	public RespuestaApi getToken(String username, String password);
	public RespuestaApi resetNewPasswordFirst(RenewPasswordFirstDTO updatePassword);
	public RespuestaApi updatePassword(UpdatePasswordDTO updatePassword);
	public RespuestaApi signOut(String token);
	public RespuestaApi refreshToken(String token);
}
