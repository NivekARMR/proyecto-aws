#!/bin/bash

yum update -y
yum install java-1.8.0 -y
yum remove java-1.7.0-openjdk -y

echo 'export host=<dns rds>.us-east-1.rds.amazonaws.com"' >> ~/.bash_profile
echo 'export port=3306' >> ~/.bash_profile
echo 'export database="geoserviciosbd"' >> ~/.bash_profile
echo 'export username="root"' >> ~/.bash_profile
echo 'export password="12345678"' >> ~/.bash_profile
echo 'export userPoolId="<userpool id>"' >> ~/.bash_profile
echo 'export clientId="<client id>"' >> ~/.bash_profile
echo 'export emailSending="<correo que envia>"' >> ~/.bash_profile
echo 'export emailDestination="<correo destino>"' >> ~/.bash_profile
echo 'export nameQueue="<nombre cola sqs>"' >> ~/.bash_profile
echo 'export aws_Region="us-east-1"' >> ~/.bash_profile
echo 'export aws_Region_Cognito="us-east-1"' >> ~/.bash_profile

source ~/.bash_profile
wget https://gitlab.com/w.marchanaranda/aws-presencial/raw/master/app-aws.jar

nohup java -Djava.security.egd=file:/dev/./urandom -jar app-aws.jar | tee output.log & 