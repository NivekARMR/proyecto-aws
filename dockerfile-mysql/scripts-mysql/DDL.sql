CREATE DATABASE IF NOT EXISTS encuestabd CHARACTER SET latin1 COLLATE latin1_swedish_ci;

USE encuestabd;

SET foreign_key_checks = 0;

DROP TABLE IF EXISTS encuesta;

CREATE TABLE Encuesta(
   	id INT(3) NOT NULL AUTO_INCREMENT,
    nombres VARCHAR(30) NOT NULL,
    apellidos VARCHAR(30) NOT NULL,
    edad INT(3) NOT NULL,
    profesion VARCHAR(30) NOT NULL,
    lugarTrabajo VARCHAR(40) NOT NULL,
    codLenguajeProgramacion VARCHAR(3) NOT NULL,
	PRIMARY KEY (`id`)
) ENGINE=INNODB;
