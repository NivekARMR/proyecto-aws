package aws.mitocode.spring.service;

import aws.mitocode.spring.model.Encuesta;

public interface IEmailService {
	
	public void sendEmail(Encuesta feedback);
	
}
