package aws.mitocode.spring.service;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import aws.mitocode.spring.model.Encuesta;

public interface IEncuestaService {

	//Page<Encuesta> obtenerDatosPaginados(Pageable pageable, String usuario, Collection<GrantedAuthority> ltaRoles);
	
	List<Encuesta> listarTodos();
	
	Page<Encuesta> listarTodos(Pageable pageable);

	Encuesta listar(Integer id);

	void registrar(Encuesta encuesta);

	void modificar(Encuesta encuesta);

	void eliminar(int id);

}
