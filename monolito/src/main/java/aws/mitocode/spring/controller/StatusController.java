package aws.mitocode.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
//@CrossOrigin
@RequestMapping("status")
public class StatusController {

	private static final Logger LOG = LoggerFactory.getLogger(StatusController.class);

	@GetMapping(value="verificar")
	public ResponseEntity<String> verificarToken(){
		LOG.info("Verificando token...devolviendo OK");
		return new ResponseEntity<String>("OK", HttpStatus.OK);
	}
}
