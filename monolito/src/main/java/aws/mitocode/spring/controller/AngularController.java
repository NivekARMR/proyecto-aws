package aws.mitocode.spring.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@CrossOrigin
public class AngularController {

	private static final Logger LOG = LoggerFactory.getLogger(AngularController.class);

	@RequestMapping(value= {"/security**","/about**","/logout**","/encuesta**","/encuestas**"})
	public String redirectTo(){
		LOG.info("Redireccionando a index...");
		return "index"; 
	}	
}
