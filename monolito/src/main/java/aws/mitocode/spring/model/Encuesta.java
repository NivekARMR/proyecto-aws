package aws.mitocode.spring.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "Encuesta")
public class Encuesta implements Serializable {

	private static final long serialVersionUID = 4215388111497938149L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name = "nombres", nullable = false)
	private String nombres;

	@Column(name = "apellidos", nullable = false)
	private String apellidos;
	
	@Column(name = "edad", nullable = false)
	private String edad;
	
	@Column(name = "profesion", nullable = false)
	private String profesion;
	
	@Column(name = "lugarTrabajo", nullable = false)
	private String lugarTrabajo;
	
	@Column(name = "codLenguajeProgramacion", nullable = false)
	private String codLenguajeProgramacion;
	
public static void main(String[] args) {
	
}
	@Override
	public String toString() {
		return "Encuesta [id=" + id + ", nombres=" + nombres + ", apellidos=" + apellidos + ", edad=" + edad + ", profesion=" + profesion + ", lugarTrabajo=" + lugarTrabajo
				+ ", codLenguajeProgramacion=" + codLenguajeProgramacion +"]";
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombres() {
		return nombres;
	}

	public void setNombres(String nombres) {
		this.nombres = nombres;
	}


	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getApellidos() {
		return apellidos;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getProfesion() {
		return profesion;
	}

	public void setProfesion(String profesion) {
		this.profesion = profesion;
	}

	public String getLugarTrabajo() {
		return lugarTrabajo;
	}

	public void setLugarTrabajo(String lugarTrabajo) {
		this.lugarTrabajo = lugarTrabajo;
	}

	public String getCodLenguajeProgramacion() {
		return codLenguajeProgramacion;
	}

	public void setCodLenguajeProgramacion(String codLenguajeProgramacion) {
		this.codLenguajeProgramacion = codLenguajeProgramacion;
	}

}
